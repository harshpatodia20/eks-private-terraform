# terraform-aws-eks

This repo gives a quick getting started guide for deploying your Amazon EKS
cluster using Hashicorp Terraform. 

# Installation

We first need to make sure we have all the necessary components installed. This
means installing:

* [AWS IAM Authenticator](https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html)
* [Terraform](https://www.terraform.io/intro/getting-started/install.html)
* [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl)

The rest of this `readme` will walk through installing these components on
Linux OS.



## What resources are created

1. VPC
2. Internet Gateway (IGW)
3. Public and Private Subnets
4. Security Groups, Route Tables and Route Table Associations
5. IAM roles, instance profiles and policies
6. An EKS Cluster
7. Autoscaling group and Launch Configuration
8. Worker Nodes in a private Subnet
9. The ConfigMap required to register Nodes with EKS
10. KUBECONFIG file to authenticate kubectl using the heptio authenticator aws binary

## Configuration

You can configure you config with the following input variables:

| Name                 | Description                       | Default       |
|----------------------|-----------------------------------|---------------|
| `cluster-name`       | The name of your EKS Cluster      | `my-cluster`  |
| `aws-region`         | The AWS Region to deploy EKS      | `us-west-2`   |
| `k8s-version`        | The desired K8s version to launch | `1.11`        |
| `node-instance-type` | Worker Node EC2 instance type     | `m4.large`    |
| `desired-capacity`   | Autoscaling Desired node capacity | `2`           |
| `max-size`           | Autoscaling Maximum node capacity | `5`           |
| `min-size`           | Autoscaling Minimum node capacity | `1`           |
| `vpc-subnet-cidr`    | Subnet CIDR                       | `10.0.0.0/16` |


> You can create a file called terraform.tfvars in the project root, to place your variables if you would like to over-ride the defaults.

## How to use this example

```bash
git clone https://git.hashedin.com/terraform-scripts-eks-aks-pks/eks.git
cd eks
```

## Remote Terraform Module

You can use this module from the Terraform registry as a remote source:

```bash
module "module" {
  source  = "vishal.gupta/eks/aws"
  version = "1.0.5"

  cluster-name       = "${var.cluster-name}"
  env                = "${var.env}"
  aws-region         = "${var.aws-region}"
  k8s-version        = "${var.k8s-version}"
  node-instance-type = "${var.node-instance-type}"
  desired-capacity   = "${var.desired-capacity}"
  max-size           = "${var.max-size}"
  min-size           = "${var.min-size}"
  vpc-subnet-cidr    = "${var.vpc-subnet-cidr}"
}
```

### IAM

The AWS credentials must be associated with a user having at least the following AWS managed IAM policies

* IAMFullAccess
* AutoScalingFullAccess
* AmazonEKSClusterPolicy
* AmazonEKSWorkerNodePolicy
* AmazonVPCFullAccess
* AmazonEKSServicePolicy
* AmazonEKS_CNI_Policy
* AmazonEC2FullAccess

In addition, you will need to create the following managed policies

*EKS*

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "eks:*"
            ],
            "Resource": "*"
        }
    ]
}
```

### Terraform

You need to run the following commands to create the resources with Terraform:

Now we're in our `eks` directory we can then run `init` to load all the
providers into the current session.

```bash
terraform init
```
```bash
....

Initializing provider plugins...

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```


Now that we have `terraform` initialized and ready for use we can run `plan`
which will show us what the config files will be creating. The output below has
been truncated for breviety.


Now that we have terraform initialized and ready for use we can run plan which will show us what the config files will be creating. The output below has been truncated for breviety.


```bash
terraform plan
terraform apply
```

```bash
Refreshing Terraform state in-memory prior to plan...
The refreshed state will be used to calculate this plan, but will not be
persisted to local or remote state storage.

data.http.workstation-external-ip: Refreshing state...
data.aws_region.current: Refreshing state...
data.aws_availability_zones.available: Refreshing state...
data.aws_ami.eks-worker: Refreshing state...

------------------------------------------------------------------------

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  + aws_autoscaling_group.demo
    ...
  + aws_eks_cluster.demo
    ...
  + aws_iam_instance_profile.demo-node
    ...
  + aws_iam_role.demo-cluster
    ...
  + aws_iam_role.demo-node
    ...
  + aws_iam_role_policy_attachment.demo-cluster-AmazonEKSClusterPolicy
    ...
  + aws_iam_role_policy_attachment.demo-cluster-AmazonEKSServicePolicy
    ...
  + aws_iam_role_policy_attachment.demo-node-AmazonEC2ContainerRegistryReadOnly
    ...
  + aws_iam_role_policy_attachment.demo-node-AmazonEKSWorkerNodePolicy
    ...
  + aws_iam_role_policy_attachment.demo-node-AmazonEKS_CNI_Policy
    ...
  + aws_internet_gateway.demo
    ...
  + aws_launch_configuration.demo
    ...
  + aws_route_table.demo
    ...
  + aws_route_table_association.demo[0]
    ...
  + aws_route_table_association.demo[1]
    ...
  + aws_security_group.demo-cluster
    ...
  + aws_security_group.demo-node
    ...
  + aws_security_group_rule.demo-cluster-ingress-node-https
    ...
  + aws_security_group_rule.demo-cluster-ingress-workstation-https
    ...
  + aws_security_group_rule.demo-node-ingress-cluster
    ...
  + aws_security_group_rule.demo-node-ingress-self
    ...
  + aws_subnet.demo[0]
    ...
  + aws_subnet.demo[1]
    ...
  + aws_vpc.demo
    ...


Plan: 24 to add, 0 to change, 0 to destroy.

------------------------------------------------------------------------

Note: You didn't specify an "-out" parameter to save this plan, so Terraform
can't guarantee that exactly these actions will be performed if
"terraform apply" is subsequently run.
```



> TIP: you should save the plan state `terraform plan -out eks-state` or even better yet, setup [remote storage](https://www.terraform.io/docs/state/remote.html) for Terraform state. You can store state in an [S3 backend](https://www.terraform.io/docs/backends/types/s3.html), with locking via DynamoDB

### Setup kubectl

Setup your `KUBECONFIG`

```bash
terraform output kubeconfig > ~/.kube/eks-cluster
export KUBECONFIG=~/.kube/eks-cluster
```

### Authorize worker nodes

Get the config from terraform output, and save it to a yaml file:

```bash
aws eks --region region update-kubeconfig --name cluster_name
terraform output config-map > config-map-aws-auth.yaml
```

Apply the config map to EKS:

```bash
kubectl apply -f config-map-aws-auth.yaml
```

You can verify the worker nodes are joining the cluster

```bash
kubectl get nodes --watch
```

### Cleaning up

You can destroy this cluster entirely by running:

```bash
terraform plan -destroy
terraform destroy  --force
```
