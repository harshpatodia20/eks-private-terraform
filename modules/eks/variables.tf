variable "cluster-name" {
  type    = "string"
  description = "Name to give the cluster"
}

variable "env" {
  description = "Environment of infrastructure"
  type        = "string"
}


variable "node-instance-type" {
  type = "string"
  description = "The aws instance type for the work nodes"
}

variable "k8s-version" {
  type        = "string"
  description = "Required K8s version"
}

variable "desired-capacity" {
  type    = "string"
  description = "Number of work nodes to create"
}

variable "aws-region" {
  default     = "us-east-2"
  type        = "string"
  description = "The AWS Region to deploy EKS"
}

variable "max-size" {
  type        = "string"
  description = "Autoscaling maximum node capacity"
}

variable "min-size" {
  type        = "string"
  description = "Autoscaling Minimum node capacity"
}

variable vpc_cidr_block {
  type = "string"
  description = "CIDR of the VPC"
}

variable "vpc_id" {
  type    = "string"
  description = "Id of the VPC to install the cluster in"
}

variable "private_subnet_ids" {
  type    = "list"
  description = "List of subnets to install the cluster in"
}



