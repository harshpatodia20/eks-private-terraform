#
# VPC Resources
resource "aws_vpc" "eks" {
  cidr_block = "${var.vpc_cidr_block}"

  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = "${
    map(
     "Name", "${var.cluster-name}-${var.env}-eks-vpc",
     "kubernetes.io/cluster/${var.cluster-name}-${var.env}", "owned",
    )
  }"
}

data "aws_availability_zones" "azs" {}

locals {
  aws_azs = "${slice(data.aws_availability_zones.azs.names, 0, var.aws_az_number)}"
}