variable "cluster-name" {
  type    = "string"
  description = "Name to give the cluster"
}

variable "env" {
  description = "Environment of infrastructure"
  type        = "string"
}

variable "aws-region" {
  default     = "us-east-2"
  type        = "string"
  description = "The AWS Region to deploy EKS"
}

variable "aws_az_number" {
  description = "How many AZs want to be used"
  type        = "string"
  default     = "3"
}

variable vpc_cidr_block {
  type = "string"
  description = "CIDR of the VPC"
}
variable "private_subnet_ids" {
  description = "Subnet IDs for EKS master"
  type        = "list"
  default     = []
}

variable "number-of-public-subnets" {
  type    = "string"
  description = "Number of subnets to create"
}

variable "number-of-private-subnets" {
  type    = "string"
  description = "Number of subnets to create"
}
