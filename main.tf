# EKS Terraform module
provider "aws" {
  region = "${var.aws-region}"
}

# terraform state file setup
# create an S3 bucket to store the state file in
module "vpc" {
  source            = "./modules/vpc"
  cluster-name      = "${var.cluster-name}"
  env               = "${var.env}"
  aws-region        = "${var.aws-region}"
  aws_az_number     = "${var.aws_az_number}"
  vpc_cidr_block    = "${var.vpc_cidr_block}"
  number-of-public-subnets  = "${var.number-of-public-subnets}"
  number-of-private-subnets  = "${var.number-of-private-subnets}"
}

module "eks" {
  source             = "./modules/eks/"
  cluster-name       = "${var.cluster-name}"
  env                = "${var.env}"
  k8s-version        = "${var.k8s-version}"
  aws-region         = "${var.aws-region}"
  vpc_id             = "${module.vpc.vpc_id}"
  vpc_cidr_block     = "${var.vpc_cidr_block}"
  private_subnet_ids   = "${module.vpc.vpc_private_subnets}"
  node-instance-type = "${var.node-instance-type}"
  desired-capacity   = "${var.desired-capacity}"
  max-size           = "${var.max-size}"
  min-size           = "${var.min-size}"

}
output "name"{
	value = "${var.cluster-name}-${var.env}"
}

#resource "aws_s3_bucket" "tfstate-bucket-storage-s3" {
#    bucket = "${var.cluster-name}-${var.env}-${var.bucket-name}"
# 
#    versioning {
#      enabled = true
#    }
# 
#    lifecycle {
#      prevent_destroy = true
#    }
# 
#    tags {
#      Name = "${var.cluster-name}-${var.env}-${var.bucket-name}"
#    }      
#}
