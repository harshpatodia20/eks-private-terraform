# General

variable "cluster-name" {
  default     = "ekscluster"
  type        = "string"
  description = "The name of your EKS Cluster"
}

variable "env" {
  description = "Environment of infrastructure"
  type        = "string"
  default     = "dev"
}

variable "k8s-version" {
  default     = "1.11"
  type        = "string"
  description = "Required K8s version"
}

### Networking
variable "aws-region" {
  default     = "us-east-2"
  type        = "string"
  description = "The AWS Region to deploy EKS"
}

variable "aws_az_number" {
  description = "How many AZs want to be used"
  type        = "string"
  default     = "3"
}

variable "bucket-name" {
  default     = "tfstate-bucket"
  type        = "string"
  description = "The name of your EKS Cluster"
}

variable "vpc_cidr_block" {
  default     = "10.0.0.0/16"
  type        = "string"
  description = "The VPC Subnet CIDR"
}

variable "number-of-public-subnets" {
  default = "2"
  type    = "string"
  description = "Number of subnets to create"
}

variable "number-of-private-subnets" {
  default = "2"
  type    = "string"
  description = "Number of subnets to create"
}

### EKS
variable "node-instance-type" {
  default     = "t2.medium"
  type        = "string"
  description = "Worker Node EC2 instance type"
}

variable "desired-capacity" {
  default     = 2
  type        = "string"
  description = "Autoscaling Desired node capacity"
}

variable "max-size" {
  default     = 5
  type        = "string"
  description = "Autoscaling maximum node capacity"
}

variable "min-size" {
  default     = 1
  type        = "string"
  description = "Autoscaling Minimum node capacity"
}

