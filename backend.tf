terraform {
  backend "s3" {
    bucket = "unisys-dev"
    key    = "tfstate"
    region = "eu-west-1"
  }
}
